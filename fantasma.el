;;; fantasma.el --- Spectral base configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Chris Shea

;; Author: Chris Shea <cmshea@gmail.com>
;; Version: 0
;; URL: https://gitlab.com/anghost/el-fantasma
;; Package-Requires: ((emacs "29.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; (require 'use-package)
;;
;; (use-package fantasma
;;   :load-path "PATH/TO/el-fantasma/"
;;   :demand t)

;;; Code:

(provide 'fantasma)

;;  ▄▄▄ .▄▄▌    ·▄▄▄ ▄▄▄·  ▐ ▄ ▄▄▄▄▄ ▄▄▄· .▄▄ · • ▌ ▄ ·.  ▄▄▄·   ;;
;;  ▀▄.▀·██•    ▐▄▄ ▐█ ▀█ •█▌▐█•██  ▐█ ▀█ ▐█ ▀. ·██ ▐███▪▐█ ▀█   ;;
;;  ▐▀▀▪▄██ ▪   █  ▪▄█▀▀█ ▐█▐▐▌ ▐█.▪▄█▀▀█ ▄▀▀▀█▄▐█ ▌▐▌▐█·▄█▀▀█   ;;
;;  ▐█▄▄▌▐█▌ ▄  ██ .▐█▪ ▐▌██▐█▌ ▐█▌·▐█▪ ▐▌▐█▄▪▐███ ██▌▐█▌▐█▪ ▐▌  ;;
;;   ▀▀▀ .▀▀▀   ▀▀▀  ▀  ▀ ▀▀ █▪ ▀▀▀  ▀  ▀  ▀▀▀▀ ▀▀  █▪▀▀▀ ▀  ▀   ;;

;;; Directories

;; Path
(add-to-list 'exec-path "/usr/local/bin")
(add-to-list 'exec-path "/usr/local/sbin")

;; Auto-saves
(defvar fantasma--auto-save-dir (file-name-concat user-emacs-directory "auto-saves/"))
(make-directory fantasma--auto-save-dir t)

(defun auto-save-file-name-p (filename)
  (string-match "^#.*#$" (file-name-nondirectory filename)))

(defun make-auto-save-file-name ()
  (concat fantasma--auto-save-dir
    (if buffer-file-name
        (concat "#" (file-name-nondirectory buffer-file-name) "#")
      (expand-file-name
        (concat "#%" (buffer-name) "#")))))

;; Backups

(add-to-list 'backup-directory-alist
             (cons
              "." (file-name-concat user-emacs-directory "backups")))

;;; My global keybindings

(setq fantasma-keybindings nil)

(defun fantasma-add-keybinding (keys command)
  "Given `keys` (will be passed to `kbd`) and a command, globally
set that binding. This will also put it in `fantasma-keybindings`,
which gets used to generate the `initial-scratch-message`."
  (add-to-list 'fantasma-keybindings (list keys command))
  (global-set-key (kbd keys) command))

;;; initial-scratch-message

(setq fantasma-splash ";;  ▄▄▄ .▄▄▌    ·▄▄▄ ▄▄▄·  ▐ ▄ ▄▄▄▄▄ ▄▄▄· .▄▄ · • ▌ ▄ ·.  ▄▄▄·   ;;
;;  ▀▄.▀·██•    ▐▄▄ ▐█ ▀█ •█▌▐█•██  ▐█ ▀█ ▐█ ▀. ·██ ▐███▪▐█ ▀█   ;;
;;  ▐▀▀▪▄██ ▪   █  ▪▄█▀▀█ ▐█▐▐▌ ▐█.▪▄█▀▀█ ▄▀▀▀█▄▐█ ▌▐▌▐█·▄█▀▀█   ;;
;;  ▐█▄▄▌▐█▌ ▄  ██ .▐█▪ ▐▌██▐█▌ ▐█▌·▐█▪ ▐▌▐█▄▪▐███ ██▌▐█▌▐█▪ ▐▌  ;;
;;   ▀▀▀ .▀▀▀   ▀▀▀  ▀  ▀ ▀▀ █▪ ▀▀▀  ▀  ▀  ▀▀▀▀ ▀▀  █▪▀▀▀ ▀  ▀   ;;")

(defun fantasma-build-initial-scratch-message (keybindings)
  (concat
   fantasma-splash
   "\n\n"
   (apply #'concat
          (seq-map (lambda (binding)
                     (format ";; %s [%s]\n"
                             (car binding)
                             (cadr binding)))
                   (seq-sort-by #'car #'string< keybindings)))
   "\n"))

(add-variable-watcher 'fantasma-keybindings
                      (lambda (_ newval _ _)
                        (setq initial-scratch-message
                              (fantasma-build-initial-scratch-message newval))))

;;; Packages

(require 'package)
(require 'use-package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(setq use-package-always-ensure t)

;;; Interface

(setq inhibit-startup-message t)
(blink-cursor-mode -1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(scroll-bar-mode -1)
(setq visible-bell t)
(setq ring-bell-function
  (lambda () (message "*beep*")))
(setq column-number-mode t)

;; delight

(use-package delight
  :config (delight '((eldoc-mode nil "eldoc"))))

;; Filenames

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; ido

(require 'ido)
(ido-mode t)

;; ivy

(use-package ivy
  :config (ivy-mode)
  :delight)

;; company

(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-tooltip-align-annotations t)
  :delight)

;; Nyan

(use-package nyan-mode
  :config
  (setq nyan-wavy-trail nil)
  (setq nyan-bar-length 12)
  (setq nyan-cat-face-number 4)
  (nyan-mode))

;;; rg

(use-package rg
  :defer t
  :ensure t
  :pin melpa-stable
  :bind (("C-c r g" . rg)))

;;; Code

(setq-default require-final-newline 't)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq-default indent-tabs-mode nil)

(setq-default tab-width 2)
(setq-default c-basic-offset 2)
(setq-default js-indent-level 2)
(setq-default css-indent-offset 2)

;; JavaScript/TypeScript

(add-to-list 'auto-mode-alist '("\\.tsx?\\'" . javascript-mode))

;; Lisp

(use-package paredit
  :hook
  ((emacs-lisp-mode scheme-mode) . paredit-mode)
  :bind (:map paredit-mode-map
              ("C-c )" . paredit-forward-slurp-sexp)
              ("C-c }" . paredit-forward-barf-sexp)
              ("C-c (" . paredit-backward-slurp-sexp)
              ("C-c {" . paredit-backward-barf-sexp)
              ("C-c w" . paredit-copy-as-kill))
  :delight " ()")

(use-package rainbow-delimiters
  :hook
  ((emacs-lisp-mode scheme-mode) . rainbow-delimiters-mode))

(use-package clojure-mode
  :pin melpa-stable)

(use-package geiser-guile
  :pin melpa-stable)

;; Rust

(use-package rust-mode
  :config
  (setq rust-format-on-save t)
  (add-to-list 'exec-path (expand-file-name "~/.cargo/bin"))
  :pin melpa-stable
  :hook
  (rust-mode . yas-minor-mode)
  :bind (:map rust-mode-map
              ([tab] . company-indent-or-complete-common)))

(use-package cargo
  :hook (rust-mode . cargo-minor-mode))

(use-package toml-mode)

;; Python

(use-package elpy
  :ensure t
  :defer t
  :pin melpa-stable
  :config
  (advice-add 'python-mode :before 'elpy-enable))

;; Go

(use-package go-mode
  :defer t
  :ensure t
  :pin melpa-stable
  :config
  (add-hook 'before-save-hook 'gofmt-before-save)
  :hook
  (go-mode . lsp-deferred)
  (go-mode . yas-minor-mode))

;; LSP

(use-package lsp-mode
  :pin melpa-stable
  :hook
  (rust-mode . lsp-deferred)
  :commands (lsp lsp-deferred)
  :config
  (setq lsp-rust-analyzer-server-command '("rust-analyzer")))

;; Docker

(use-package dockerfile-mode)

;; Markdown

(use-package markdown-mode
  :config
  (setq markdown-content-type "text/html")
  (setq markdown-coding-system 'utf-8)
  (add-hook 'markdown-mode-hook 'auto-fill-mode)
  :config
  (add-to-list 'markdown-css-paths
               (file-name-concat
                (file-name-directory (locate-library "fantasma"))
                "markdown.css")))

;; Yaml

(use-package yaml-mode
  :pin melpa-stable)

;; Magit

(use-package magit
  :config
  (setq magit-last-seen-setup-instructions "1.4.0"))

(fantasma-add-keybinding "C-c m s" #'magit-status)

;; Terraform

(use-package terraform-mode
  :pin melpa-stable)

;;; Shell

(defun fantasma-rename-shell-buffer ()
  (interactive)
  (let ((directory-basename (file-name-base
                             (directory-file-name
                              default-directory))))
    (rename-buffer (concat "*" directory-basename "-shell*") t)))

(add-hook 'shell-mode-hook #'fantasma-rename-shell-buffer)

(fantasma-add-keybinding "C-c s r" #'fantasma-rename-shell-buffer)

;;; Extra keybindings

(fantasma-add-keybinding "C-c b p" #'browse-url-at-point)
(fantasma-add-keybinding "C-c b b" #'browse-url-of-buffer)
(fantasma-add-keybinding "C-c f f" #'find-file-at-point)
(fantasma-add-keybinding "C-c f b" #'flymake-show-buffer-diagnostics)
(fantasma-add-keybinding "C-c i c" #'insert-char)

;;; Server

(server-start)

;;; Themes

(add-to-list 'custom-theme-load-path
             (file-name-concat
              (file-name-directory (locate-library "fantasma"))
              "themes"))

;;; Actually... let's just use ef-summer?
(use-package ef-themes
  :config
  (setq ef-summer-palette-overrides
        '((comment fg-alt)))
  (ef-themes-select 'ef-summer))

;;; Font

(defun fantasma-set-fonts (frame)
  (select-frame frame)

  (if (member "Fantasque Sans Mono" (font-family-list))
      (set-frame-font "Fantasque Sans Mono")
    (message "Please install Fantasque Sans Mono!"))

  (if (member "Victor Mono" (font-family-list))
      (progn
        (add-to-list 'face-font-rescale-alist '("Victor Mono" . 0.85))
        (set-face-attribute 'italic nil
                            :family "Victor Mono"
                            :weight 'semi-bold))
    (message "Please install Victor Mono!")))

;;;; For some systems, there's no frame yet when this is happening, so
;;;; we need to set a hook for when a frame is created:
(add-hook 'after-make-frame-functions #'fantasma-set-fonts)

;;;; For other systems, there's a frame already, so we have to do it
;;;; right away.
(if-let (current-frame (selected-frame))
    (fantasma-set-fonts current-frame))

(provide 'fantasma)

;;; fantasma.el ends here
