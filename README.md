# Emacs configuration

This is my base Emacs configuration. There are many like it, but this
one is mine.

## Set up

1. Clone the repo
2. Add this to `~/.emacs.d/init.el`:

```
(require 'use-package)

(use-package fantasma
  :load-path "PATH/TO/el-fantasma/"
  :demand t)
```

## Themes

If you want a theme:

```
(load-theme 'flowers)
```

## History

When I upgraded to Emacs 24 in 2012 I decided I'd start using
packages instead of copying and pasting from EmacsWiki, so I started a
new configuration from scratch.

Then I read and really enjoyed some org-mode literate configs, and
tried that out in 2014.

In a bout of cabin fever in 2020 I pared it way back and dropped the
literate style.

I imagine in 2024 I will burn it down again and see what grows from
the ashes.

Hey look! It's 2024, and now I'm moving everything out of .emacs.d and
into a separate directory.
